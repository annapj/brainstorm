<?php

define("WEBMASTER_EMAIL", 'rollthemes.jlsp@gmail.com');

$error = false;
$fields = array( 'name', 'email', 'message' );

foreach ( $fields as $field ) {
	if ( empty($_POST[$field]) || trim($_POST[$field]) == '' )
		$error = true;
}

if ( !$error ) {

	$name = stripslashes($_POST['name']);
	$email = trim($_POST['email']);
	$message = stripslashes($_POST['message']);

	$mail = @mail(WEBMASTER_EMAIL, "You have a new message.", $message,
		 "From: " . $name . " <" . $email . ">\r\n"
		."Reply-To: " . $email . "\r\n"
		."X-Mailer: PHP/" . phpversion());

	if ( $mail ) {
		echo 'Success';
	} else {
		echo $error;
	}
}

?>